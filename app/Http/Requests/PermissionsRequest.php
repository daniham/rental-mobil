<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermissionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'guard_name' => 'required', 'string', 'email', 'max:255',
            'group_name' => 'required', 'string', 'email', 'max:255',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name can not be blank value !!',
            'guard_name.unique' => 'Guard Name can not be blank value !!',
            'group_name.required' => 'Guard Group can not be blank value !!',            
        ];
    }
}
