<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;

class CompanyController extends Controller
{
    public function index()
    {
        $company = Company::all();
        return view('backend.pages.company.index', compact('company'));
    }

    public function edit($id)
    {
        $company = Company::find($id);

        return view('backend.pages.company.edit',compact('company','id'));
    }

    public function update(Request $request, $id)
    {
            $request->validate([
                'company_name' => 'required',
                'company_address' => 'required',
                'company_email' => 'required', 'string', 'email', 'max:255', 'unique:companies',
                'contact_number' => 'required'
                ]);

            $company = Company::find($id);
            $company->company_name = $request->company_name;
            $company->company_address = $request->company_address;
            $company->company_email = $request->company_email;
            $company->contact_number = $request->contact_number;
            
            $company->save();
            return redirect()->route('config.company.edit',1)
            ->with('sukses','Data telah Diubah.');
    }
}
