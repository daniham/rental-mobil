<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Permissions;
use App\Http\Requests\PermissionsRequest;

class PermissionsController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }
    public function index()
    {
        if (is_null($this->user) || !$this->user->can('permisions.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any admin !');
        }

        $permisions = Permissions::all();
        return view('backend.pages.permisions.index', compact('permisions'));
    }
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('permisions.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
        return view('backend.pages.permisions.create');
    }
    public function store(PermissionsRequest $request)
    {
        if (is_null($this->user) || !$this->user->can('permisions.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
        $permisions= new Permissions();
        $permisions->name = $request->name;
        $permisions->guard_name = $request->guard_name;
        $permisions->group_name = $request->group_name;
        // $permisions->created_by = $this->user->name;
        $permisions->save();

        session()->flash('success', 'Permissions has been created !!');
        return redirect()->route('admin.permisions.index');
    }
    public function show($id)
    {

        $permisions = Permissions::find($id);

        return view('backend.pages.permisions.show',compact('permisions'));
    }
    public function edit(int $id)
    {
        if (is_null($this->user) || !$this->user->can('listcateg.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any admin !');
        }
        $permisions = Permissions::find($id);
        return view('backend.pages.permisions.edit', compact('permisions'));
    }
    public function update(Request $request, $id)
    {

        $permisions = Permissions::find($id);
        // Validation Data
        $request->validate([
            'name' => 'required',
            'guard_name' => 'required',
            'group_name' => 'required',
        ]);

        $permisions->name = $request->name;
        $permisions->guard_name = $request->guard_name;
        $permisions->group_name = $request->group_name;

        $permisions->save();

        session()->flash('success', 'Permissions has been updated !!');
        return redirect()->route('admin.permisions.index');
    }
    public function destroy($id)
    {
        $permisions = Permissions::find($id);
        if (!is_null($permisions)) {
            $permisions->delete();
        }

        session()->flash('success', 'Permissions has been deleted !!');
        return redirect()->route('admin.permisions.index');
    }
    public function importData(Request $request){
        if (is_null($this->user) || !$this->user->can('permisions.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
        $this->validate($request, [
            'uploaded_file' => 'required|file|mimes:xls,xlsx'
        ]);
        $the_file = $request->file('uploaded_file');
        try{
            $spreadsheet = IOFactory::load($the_file->getRealPath());
            $sheet        = $spreadsheet->getActiveSheet();
            $row_limit    = $sheet->getHighestDataRow();
            $column_limit = $sheet->getHighestDataColumn();
            $row_range    = range( 2, $row_limit );
            $column_range = range( 'C', $column_limit );
            $startcount = 2;
            $data = array();
            foreach ( $row_range as $row ) {
                $data[] = [
                    'name' =>$sheet->getCell( 'A' . $row )->getValue(),
                    'guard_name' => $sheet->getCell( 'B' . $row )->getValue(),
                    'group_name' => $sheet->getCell( 'C' . $row )->getValue(),
                ];
                $startcount++;
            }
            DB::table('permissions')->insert($data);
        } catch (Exception $e) {
            $error_code = $e->errorInfo[1];
            return back()->withErrors('There was a problem uploading the data!');
        }
        return back()->withSuccess('Great! Data has been successfully uploaded.');
    }
}
