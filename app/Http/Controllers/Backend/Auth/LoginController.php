<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::ADMIN_DASHBOARD;

    /**
     * show login form for admin guard
     *
     * @return void
     */
    public function showLoginForm()
    {
        return view('backend.auth.login');
    }
    public function showRegisterForm()
    {
        return view('backend.auth.register');
    }


    /**
     * login admin
     *
     * @param Request $request
     * @return void
     */


    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'username' => 'required|string|max:255',
            'sim_no' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'telp' => 'required|string|max:13',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:8|confirmed',
        ], [
            'name.required' => 'nama tidak boleh kosong !!',
            'username.required' => 'username tidak boleh kosong !!',
            'address.required' => 'alamat tidak boleh kosong !!',
            'sim_no.required' => 'Nomor Sim tidak boleh kosong !!',
            'telp.required' => 'telp tidak boleh kosong !!',
            'telp.max' => 'telp maksimal 13 angka !!',
            'email.required' => 'email tidak boleh kosong !!',
            'email.unique' => 'email sudah di gunakan sebelumnya oleh pengguna lain!!',
            'password.required' => 'password tidak boleh kosong!!',
        ]);

        DB::beginTransaction();

        try {
            // Buat data admin baru
            $admin = Admin::create([
                'name' => $request->name,
                'address' => $request->address ?? '',
                'telp' => '+62' . ltrim($request->telp, '0'),
                'email' => $request->email,
                'sim_no' => $request->sim_no,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'realpassword' => $request->password,
            ]);

            // Assign role 'user' ke admin baru
            $roleUser = Role::where('name', 'user')->first();
            if ($roleUser) {
                $admin->assignRole($roleUser);
            }

            DB::commit();

            // Redirect ke halaman login dengan pesan sukses
            return redirect()->route('admin.login')->with('success', 'Registration successful! You can now login.');
        } catch (\Exception $e) {
            DB::rollBack();

            // Handle the error (e.g., log it and/or show a user-friendly message)
            return redirect()->route('admin.register')->with('error', 'Registration failed! Please try again.');
        }
    }

     

    public function login(Request $request)
    {
        // Validate Login Data
        $request->validate([
            'email' => 'required|max:50',
            'password' => 'required',
        ]);

        // Attempt to login
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // Redirect to dashboard
            session()->flash('success', 'Successully Logged in !');
            return redirect()->route('admin.dashboard');
        } else {
            // Search using username
            if (Auth::guard('admin')->attempt(['username' => $request->email, 'password' => $request->password], $request->remember)) {
                session()->flash('success', 'Successully Logged in !');
                return redirect()->route('admin.dashboard');
            }
            // error
            session()->flash('error', 'Invalid email and password');
            return back();
        }
    }

    /**
     * logout admin guard
     *
     * @return void
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
