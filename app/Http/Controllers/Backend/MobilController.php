<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\Mobil;
use App\Helpers\CurrencyHelper;  // Import the helper

class MobilController extends Controller
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if (is_null($this->user) || !$this->user->can('mobil.view')) {
            abort(403, 'Sorry !! You are Unauthorized to view any admin !');
        }

        $query = Mobil::orderBy('id', 'DESC');

        if ($this->user->is_admin != 1) {
            $query->where('created_by', $this->user->name);
        }

        foreach ($request->all() as $key => $value) {
            if (!is_null($value) && $key != '_token') {
                $query->where($key, $value);
            }
        }

        $mobil = $query->get();
        return view('backend.pages.mobil.index', compact('mobil'));
    }
    public function create()
    {
        if (is_null($this->user) || !$this->user->can('mobil.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
      
        return view('backend.pages.mobil.create');
    }
    public function store(Request $request)
    {
        if (is_null($this->user) || !$this->user->can('mobil.create')) {
            abort(403, 'Sorry !! You are Unauthorized to create any admin !');
        }
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'merek' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'plat_nomor' => 'required|string|max:255',
            'tarif' => 'required', // Validasi untuk angka desimal dengan maksimal 2 angka desimal
        ], [
            'image.required' => 'Gambar tidak boleh kosong!',
            'image.image' => 'File harus berupa gambar!',
            'image.mimes' => 'Gambar harus berformat: jpeg, png, jpg, gif, svg!',
            'image.max' => 'Ukuran gambar maksimal adalah 2MB!',
            'merek.required' => 'Merek tidak boleh kosong!',
            'model.required' => 'Model tidak boleh kosong!',
            'plat_nomor.required' => 'Plat nomor tidak boleh kosong!',
            'tarif.required' => 'Tarif tidak boleh kosong!',
        ]);
        DB::beginTransaction();

        try {
           
            // Proses penyimpanan gambar dan data lainnya
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('storage/images'), $imageName);
        
                // Simpan data ke database
                $mobil= new Mobil();
                $mobil->image = $imageName;
                $mobil->merek = $request->merek;
                $mobil->model = $request->model;
                $mobil->plat_nomor = $request->plat_nomor;
                $mobil->tarif =  CurrencyHelper::convertCurrencyToDecimal($request->tarif);
                $mobil->created_by = $this->user->name;
                $mobil->save();
            }
            
            DB::commit();

            // Redirect ke halaman login dengan pesan sukses
            return redirect()->route('admin.master.mobil.edit',$mobil->getKey())->with('success', 'Data mobil successful save! You can now login.');
        } catch (\Exception $e) {
            DB::rollBack();

            // Handle the error (e.g., log it and/or show a user-friendly message)
            return redirect()->route('admin.master.mobil.create')->with('error', 'Data mobil failed save! Please try again.');
        }        
    }
    public function edit(int $id)
    {
        if (is_null($this->user) || !$this->user->can('mobil.edit')) {
            abort(403, 'Sorry !! You are Unauthorized to edit any admin !');
        }
        $mobil = Mobil::find($id);
      
        return view('backend.pages.mobil.edit', compact('mobil'));
    }

    public function destroy($id)
    {
        if (is_null($this->user) || !$this->user->can('mobil.delete')) {
            abort(403, 'Sorry !! You are Unauthorized to delete any admin !');
        }
        $mobil = Mobil::find($id);
        if (!is_null($mobil)) {
            $mobil->delete();
        }
        session()->flash('success', 'Mobil Berhasil di delete Permanen!!');
        return redirect()->route('admin.master.mobil.index');    
    }

    public function delete($id)
    {
        if (is_null($this->user) || !$this->user->can('mobil.move')) {
            abort(403, 'Sorry !! You are Unauthorized to move any admin !');
        }
        $mobil = Mobil::find($id);
        $mobil->status=0;
        $mobil->save();
        session()->flash('success', 'Data Mobil Telah Di non aktifkan !!');
        return redirect()->route('admin.master.mobil.index');  
    }

    public function restore($id)
    {
        if (is_null($this->user) || !$this->user->can('mobil.restore')) {
            abort(403, 'Sorry !! You are Unauthorized to restore any admin !');
        }
       
        $mobil = Mobil::find($id);
        $mobil->status=1;
        $mobil->save();


        session()->flash('success', 'Data Mobil Telah Activate !!');
        return redirect()->route('admin.master.mobil.index');
    }
}
