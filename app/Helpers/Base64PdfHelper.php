<?php

namespace App\Helpers;

class Base64PdfHelper
{
    public static function getImagePdf($imagePath)
    {
        $url = ENV('APP_MODE', 'APP_LOCAL')=='LOCAL' ?  ENV('APP_LOCAL', ENV('APP_LOCAL')): (ENV('APP_MODE', 'DEV') == 'DEV' ? ENV('APP_DEV',ENV('APP_DEV')) : ENV('APP_PROD',ENV('APP_PROD')));

        $fullImagePath = public_path('storage/picture/' . $imagePath);

        if (is_file($fullImagePath)) { // Tambahkan pengecekan apakah itu adalah file
            return base64_encode(file_get_contents($fullImagePath));
        } else {
            // Handle jika path tidak menuju ke file yang valid
            return null;
        }
    }
}

