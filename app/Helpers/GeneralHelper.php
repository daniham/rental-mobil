<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class GeneralHelper
{
    public $user;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::guard('admin')->user();
            return $next($request);
        });
    }

    public static function calculateChangePercentage($initial, $final): float
    {
        if ($initial > $final) {
            return self::calculateDecrementPercentage($initial, $final);
        }

        if ($final > $initial) {
            return self::calculateIncrementPercentage($initial, $final);
        }

        return 0.00;
    }

    public static function calculateIncrementPercentage($initial, $final): float
    {
        return round(($final - $initial) / $final * 100, 2);
    }

    public static function calculateDecrementPercentage($initial, $final): float
    {
        return round(($initial - $final) / $initial * -100, 2);
    }

    public static function greeting(): string
    {
        

        $greetingLang = 'evening';
        $currentHour = now()->hour;

        if ($currentHour < 4) {
            $greetingLang = 'night';
        } elseif ($currentHour < 11) {
            $greetingLang = 'morning';
        } elseif ($currentHour < 15) {
            $greetingLang = 'afternoon';
        } elseif ($currentHour > 20) {
            $greetingLang = 'night';
        }

        return __( 'dashboard.greeting.' . $greetingLang);
    }
}
