<?php

namespace App\Helpers;
use App\Helpers\CurrencyHelper;  // Import the helper

class ProcessValueHelper
{
    public static function processValue($value)
    {
        if (is_array($value)) {
            $value = reset($value);
        }
        if (!is_null($value) && $value !== '') {
            if (strpos($value, ',') !== false) {
                return CurrencyHelper::convertCurrencyToDecimal($value);
            } else {
                // Hapus simbol "Rp" dan spasi
                $result = str_replace(["Rp", " "], "", $value);
                // Ganti titik dengan kosong
                $result = str_replace(".", "", $result);
                // Ubah ke format float dan tambahkan tiga digit desimal
                return number_format(floatval($result), 3, '.', '');
            }
        }
        return null;
    }
}

