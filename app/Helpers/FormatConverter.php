<?php

namespace App\Helpers;

class FormatConverter
{
    public static function convertCurrencyToDatabaseFormat($value)
    {
        // Menghapus simbol mata uang dan spasi
        $cleanedValue = str_replace(['Rp', '.', ' '], '', $value);

        // Mengganti koma dengan titik jika ada
        $cleanedValue = str_replace(',', '.', $cleanedValue);

        // Jika nilai hanya berupa angka, tambahkan .000 di belakangnya
        if (is_numeric($cleanedValue)) {
            $cleanedValue .= '.000';
        }

        // Pastikan nilainya adalah float
        $convertedValue = floatval($cleanedValue);

        return $convertedValue;
    }
}