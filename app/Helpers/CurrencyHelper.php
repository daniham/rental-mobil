<?php

namespace App\Helpers;

class CurrencyHelper
{
    public static function convertCurrencyToDecimal($value)
    {
        // Remove the "Rp " prefix
        $value = str_replace('Rp ', '', $value);
        // Remove thousand separators
        $value = str_replace('.', '', $value);
        // Replace comma with dot for decimal point
        $value = str_replace(',', '.', $value);
        // Convert to float
        return floatval($value);
    }
}

if (!function_exists('formatNumber')) {
    function formatNumber($number) {
        // Bagi angka dengan 1000 untuk memindahkan tiga digit dari belakang sebagai bagian dari pecahan desimal
        $formattedNumber = $number / 1000;
        // Gunakan number_format() untuk memformat angka dengan menambahkan titik sebagai pemisah ribuan dan menghilangkan tanda koma sebagai pemisah ribuan
        $formattedNumber = number_format($formattedNumber, 3, '.', '');
        return $formattedNumber;
    }
}
