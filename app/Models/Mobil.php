<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
class Mobil extends Model
{
    use Notifiable, HasRoles,HasFactory;
    protected $fillable = [
        'merek',
        'model',
        'plat_nomor',
        'tarif',
        'status',
        'image',
        'created_by',
        'updated_by',
    ];
}
