<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
class Permissions extends Model
{
    use Notifiable, HasRoles,HasFactory;
    protected $fillable = [
        'name', 'guard_name','group_name',
    ];
}
