<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Company extends Model
{
    use Notifiable, HasRoles,HasFactory;

    protected $fillable = [
        'company_name', 'company_address', 'company_email','contact_number',
    ];
}
