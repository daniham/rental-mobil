<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Str;

class PengembalianMobil extends Model
{
    use Notifiable, HasRoles,HasFactory;
    protected $fillable = [
        'tgl_transaksi',
        'kdPinjams',
        'denda',
        'otherFee',
        'status_pinjam',
        'status_bayar',
        'created_by',
        'updated_by',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->id = self::generateUniqueString();
        });
    }

    private static function generateUniqueString()
    {
        $uniqueString = Str::random(64); // Adjust length as needed

        // Ensure the unique string is truly unique by checking the database
        while (self::where('id', $uniqueString)->exists()) {
            $uniqueString = Str::random(64);
        }

        return $uniqueString;
    }
}
