<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam_mobils', function (Blueprint $table) {
            $table->string('id', 64)->primary(); // Primary key
            $table->unsignedBigInteger('UserId');
            $table->unsignedBigInteger('mobilId');
            $table->datetime('tgl_transaksi')->nullable();
            $table->datetime('tgl_pinjam')->nullable();
            $table->datetime('tgl_kembali')->nullable();
            $table->tinyinteger('status')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            
            // Foreign key constraints
            $table->foreign('UserId')->references('id')->on('admins')->onDelete('cascade');
            $table->foreign('mobilId')->references('id')->on('mobils')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam_mobils');
    }
};
