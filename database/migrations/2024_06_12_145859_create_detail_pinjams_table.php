<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pinjams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kdPinjams', 64); // Change this to match the 'id' column in 'pinjam_mobils'
            $table->integer('discount')->default(0); 
            $table->decimal('subTotalBayar', 15, 2)->default(0.00);
            $table->decimal('totalBayar', 15, 2)->default(0.00);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            
            // Foreign key constraints
            $table->foreign('kdPinjams')->references('id')->on('pinjam_mobils')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_pinjams');
    }
};
