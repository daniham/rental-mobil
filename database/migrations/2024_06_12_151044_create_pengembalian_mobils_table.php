<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengembalian_mobils', function (Blueprint $table) {
            $table->string('id', 64)->primary(); // Primary key
            $table->datetime('tgl_transaksi')->nullable();
            $table->string('kdPinjams', 64); // Match type and length with 'pinjam_mobils.id'
            $table->decimal('denda', 15, 2)->default(0.00)->nullable();
            $table->decimal('otherFee', 15, 2)->default(0.00)->nullable();
            $table->tinyInteger('status_pinjam')->default(0); // 1 = paid, 0 = not paid
            $table->tinyInteger('status_bayar')->default(0); // 1 = paid, 0 = not paid
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
            
            // Foreign key constraints
            $table->foreign('kdPinjams')->references('id')->on('pinjam_mobils')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengembalian_mobils');
    }
};
