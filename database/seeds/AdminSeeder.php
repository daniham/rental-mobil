<?php

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::where('username', 'superadmin')->first();

        if (is_null($admin)) {
            $admin           = new Admin();
            $admin->name     = "Super Admin";
            $admin->address     = "Jatinagor Sukawening ,Sumedang Jawa Barat";
            $admin->telp     = "85715329867";
            $admin->sim_no     = "7541-78946-224";
            $admin->email    = "superadmin@example.com";
            $admin->username = "superadmin";
            $admin->password = Hash::make('12345678');
            $admin->realpassword = '12345678';
            $admin->is_admin =  1;
            $admin->save();
        }
    }
}
