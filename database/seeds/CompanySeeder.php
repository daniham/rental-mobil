<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Company::create([
            'company_name'   =>'DMI Holding',
            'company_address'  =>'Jl.Soekarno Hatta 600',
            'company_email' => "DMI@gmail.com",
            'contact_number' => "08512978812",
        ]);
    }
}
