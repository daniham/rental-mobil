<?php
use App\Models\Admin;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class RolePermissionSeeder.
 *
 * @see https://spatie.be/docs/laravel-permission/v5/basic-usage/multiple-guards
 *
 * @package App\Database\Seeds
 */
class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Permission List as array
        $permissions = [
            [
                'group_name' => 'mobil',
                'permissions' => [
                    'mobil.create',
                    'mobil.view',
                    'mobil.edit',
                    'mobil.delete',
                    'mobil.move',
                    'mobil.restore',
                    'mobil.approve',
                ]
            ],
            [
                'group_name' => 'pinjams',
                'permissions' => [
                    'pinjams.create',
                    'pinjams.view',
                    'pinjams.edit',
                    'pinjams.delete',
                    'pinjams.move',
                    'pinjams.restore',
                    'pinjams.approve',
                ]
            ],
            [
                'group_name' => 'kembali',
                'permissions' => [
                    'kembali.create',
                    'kembali.view',
                    'kembali.edit',
                    'kembali.delete',
                    'kembali.move',
                    'kembali.restore',
                    'kembali.approve',
                ]
            ],
            [
                'group_name' => 'permisions',
                'permissions' => [
                    'permisions.create',
                    'permisions.view',
                    'permisions.edit',
                    'permisions.delete',
                    'permisions.approve',
                    'permisions.move',
                    'permisions.restore',
                ]
            ],
            [
                'group_name' => 'company',
                'permissions' => [
                    'company.view',
                    'company.edit',
                ]
            ],
            [
                'group_name' => 'dashboard',
                'permissions' => [
                    'dashboard.view',
                    'dashboard.edit',
                ]
            ],
            [
                'group_name' => 'blog',
                'permissions' => [
                    'blog.create',
                    'blog.view',
                    'blog.edit',
                    'blog.delete',
                    'blog.approve',
                ]
            ],
            [
                'group_name' => 'admin',
                'permissions' => [
                    'admin.create',
                    'admin.view',
                    'admin.edit',
                    'admin.delete',
                    'admin.approve',
                ]
            ],
            [
                'group_name' => 'role',
                'permissions' => [
                    'role.create',
                    'role.view',
                    'role.edit',
                    'role.delete',
                    'role.approve',
                ]
            ],
            [
                'group_name' => 'profile',
                'permissions' => [
                    'profile.view',
                    'profile.edit',
                ]
            ],
        ];

        // Create and Assign Permissions for Super Admin
        $roleSuperAdmin = Role::create(['name' => 'superadmin', 'guard_name' => 'admin']);

        foreach ($permissions as $group) {
            foreach ($group['permissions'] as $permissionName) {
                $permission = Permission::create([
                    'name' => $permissionName,
                    'group_name' => $group['group_name'],
                    'guard_name' => 'admin'
                ]);
                $roleSuperAdmin->givePermissionTo($permission);
                $permission->assignRole($roleSuperAdmin);
            }
        }

        // Assign super admin role permission to superadmin user
        $admin = Admin::where('username', 'superadmin')->first();
        if ($admin) {
            $admin->assignRole($roleSuperAdmin);
        }

        // Create User Role and Assign Specific Permissions
        $roleUser = Role::create(['name' => 'user', 'guard_name' => 'admin']);
        $userPermissionGroups = ['mobil', 'pinjams', 'kembali', 'dashboard', 'profile'];

        foreach ($userPermissionGroups as $groupName) {
            $groupPermissions = array_filter($permissions, function($group) use ($groupName) {
                return $group['group_name'] == $groupName;
            });

            if (!empty($groupPermissions)) {
                $groupPermissions = array_shift($groupPermissions)['permissions'];
                foreach ($groupPermissions as $permissionName) {
                    $permission = Permission::where('name', $permissionName)
                        ->where('guard_name', 'admin')
                        ->first();
                    if ($permission) {
                        $roleUser->givePermissionTo($permission);
                    }
                }
            }
        }

        // Assign user role to specific admin user if needed
        $userAdmin = Admin::where('username', 'useradmin')->first();
        if ($userAdmin) {
            $userAdmin->assignRole($roleUser);
        }
    }
}
