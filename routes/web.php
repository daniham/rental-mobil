<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@redirectAdmin')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Admin routes
 */
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Backend\DashboardController@index')->name('admin.dashboard');
    Route::resource('roles', 'Backend\RolesController', ['names' => 'admin.roles']);
    Route::resource('users', 'Backend\UsersController', ['names' => 'admin.users']);
    Route::resource('admins', 'Backend\AdminsController', ['names' => 'admin.admins']);

    // compro
    Route::resource('company-profile', 'Backend\CompanyController', ['names' => 'config.company']);

    //mobil
    Route::resource('master-mobil', "Backend\MobilController", ["names" => "admin.master.mobil"]);
    Route::put('/delete/mobil/{id}', 'Backend\MobilController@delete')->name('admin.softDelete.mobil');
    Route::put('/restore/mobil/{id}', 'Backend\MobilController@restore')->name('admin.restore.mobil');

    // permisions
    Route::resource('config/permisions','Backend\PermissionsController', ['names' => 'admin.permisions']);
    Route::post('permissions/import', 'Backend\PermissionsController@importData')->name('admin.permisions.file-import');

    // Login Routes
    Route::get('/login', 'Backend\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/register', 'Backend\Auth\LoginController@showRegisterForm')->name('admin.register');
    Route::post('/login/submit', 'Backend\Auth\LoginController@login')->name('admin.login.submit');
    Route::post('/admin/register', 'Backend\Auth\LoginController@register')->name('admin.register.submit');

    // Logout Routes
    Route::post('/logout/submit', 'Backend\Auth\LoginController@logout')->name('admin.logout.submit');

    // Forget Password Routes
    Route::get('/password/reset', 'Backend\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset/submit', 'Backend\Auth\ForgotPasswordController@reset')->name('admin.password.update');
});
