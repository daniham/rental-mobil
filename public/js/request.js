$("document").ready(function () {
    setInterval(() => {
        let url = $("#notif-url").data("url");
        $.get(url, (data) => {
            const {
                appnf,
                journalmanual,
                adjustmentapp,
                stockopnameapp,
                pricechangeapp
            } = data;

            // ================================ NAMA CLASS UNIK MENU SIDEBAR ================================
            setNotification("appnf", appnf);
            setNotification("journalmanual", journalmanual);
            setNotification("adjustmentapp", adjustmentapp);
            setNotification("stockopnameapp", stockopnameapp);
            setNotification("pricechangeapp", pricechangeapp);
        });
    }, 1000);
});

// set span
function setNotification(cls, qty) {
    if (qty > 0) {
        $("." + cls)
            .removeClass("text-black")
            .addClass("text-black")
            .html(shortQty(qty));
    } else {
        $("." + cls)
            .removeClass("text-black")
            .html("");
    }
}

// short quantity
function shortQty(qty) {
    if (qty > 3000) {
        return qty / 3000 + "K";
    } else {
        return qty;
    }
}