@extends('backend.auth.auth_master')

@section('auth_title')
    Register | Admin Panel
@endsection

@section('auth-content')
    <!-- register area start -->
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100" style="border-radius: 30px;">
                <form method="POST" action="{{ route('admin.register.submit') }}">
                    @csrf

                    <div class="login-form-head" style="border-radius: 15px;">
                        <h4>Register</h4>
                        <p>Hello there, please fill in the form to register</p>
                    </div>
                    <div class="login-form-body">
                        @include('backend.layouts.partials.messages')

                        <div class="form-gp">
                            <label for="name">Nama Lengkap</label>
                            <input type="text" id="name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            <i class="fa fa-user" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <label for="address">Alamat</label>
                            <textarea type="text" class="form-control"  rows="7" name="address"  value="{{ old('address') }}"></textarea>
                            <div class="text-danger"></div>
                            @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <label for="telp">Nomor Telepon</label>
                            <input type="text" id="telp" name="telp" value="{{ old('telp') }}" class="form-control" required autocomplete="telp" autofocus inputmode="tel">
                            <i class="fa fa-phone" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('telp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="form-gp">
                            <label for="sim_no">Nomor Sim</label>
                            <input type="text" id="sim_no" name="sim_no" value="{{ old('sim_no') }}" required autocomplete="sim_no" autofocus>
                            <i class="fa fa-file" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('sim_no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <label for="email">Email address</label>
                            <input type="email" id="email" name="email" value="{{ old('email') }}" required autocomplete="email">
                            <i class="fa fa-envelope" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <label for="username">Username</label>
                            <input type="text" id="username" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>
                            <i class="fa fa-user" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <label for="password">Password</label>
                            <div class="input-group" id="show_hide_password">
                                <input type="password" id="password" name="password" required autocomplete="new-password">
                                <i class="fa fa-eye-slash" onclick="showHidePassword(this)" data-show="false" 
                                style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff; right: 2px;"></i>
                            </div>
                            <div class="text-danger"></div>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-gp">
                            <label for="password_confirmation">Confirm Password</label>
                            <div class="input-group" id="show_hide_confirm_password">
                                <input type="password" id="password_confirmation" name="password_confirmation" required autocomplete="new-password">
                                <i class="fa fa-eye-slash" onclick="showHidePassword(this)" data-show="false" 
                                style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff; right: 2px;"></i>
                            </div>
                            <div class="text-danger"></div>
                            @error('password_confirmation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="row mb-4 rmber-area">
                            <div class="col-12 text-right">
                                <a href="{{ route('admin.login') }}">Already have an account? Login</a>
                            </div>
                        </div>

                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Register <i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- register area end -->
@endsection

@section('scripts')

<script>
    function showHidePassword(element) {
        let status = $(element).data('show');
        if (!status) {
            $(element).parent().find('input').attr('type', 'text');
            $(element).parent().find('i').attr('class', 'fa fa-eye');
        } else {
            $(element).parent().find('input').attr('type', 'password');
            $(element).parent().find('i').attr('class', 'fa fa-eye-slash');
        }
        $(element).data('show', !status);
    }
</script>
@endsection
