@extends('backend.auth.auth_master')

@section('auth_title')
    Login | Admin Panel
@endsection

@section('auth-content')
    <!-- login area start -->
    <div class="login-area">
        <div class="container">
            <div class="login-box ptb--100" style="border-radius: 30px;">
                <form method="POST" action="{{ route('admin.login.submit') }}">
                    @csrf

                    <div class="login-form-head">
                        <h4>Log In Form</h4>
                        <p>Hello there, Sign in and start Rental Mobil</p>
                    </div>
                    <div class="login-form-body">
                        @include('backend.layouts.partials.messages')
                        <div class="form-gp">
                            <label for="exampleInputEmail1">Email Atau Username</label>
                            <input type="text" id="exampleInputEmail1" name="email" autocomplete="off">
                            <i class="fa fa-envelope" style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff;"></i>
                            <div class="text-danger"></div>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-gp">
                            <div class="input-group" id="show_hide_password">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" id="exampleInputPassword1" name="password" autocomplete="off">
                                <i class="fa fa-eye-slash" onclick="showHidePassword(this)" data-show="false" 
                                style="position: absolute; right: 10px; cursor: pointer; top: 50%; font-size: 18pt; transform: translate(0, -50%); color: #7e74ff; right: 2px;"></i>
                                <div class="text-danger"></div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-gp">
                            <input type="hidden" id="device_token" name="device_token">
                        </div>
                        <div class="row mb-4 rmber-area">
                            <div class="col-6">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="customControlAutosizing" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="customControlAutosizing">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-6 text-right">
                                <a href="#">Forgot Password?</a>
                            </div>
                            <hr style="height: 5px; background-color: purple; border: none;">
                            <div class="col-12 text-center submit-btn-area">
                                <a href="{{ route('admin.register') }}" style="border-radius: 15px;"></i>Register Pengguna</a>
                            </div>
                            {{-- <div class="col-6 text-right">
                                <a href="{{ route('admin.forgotPassword') }}">Forgot Password?</a>
                            </div> --}}
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Sign In <i class="ti-arrow-right"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- login area end -->
@endsection

@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/@fingerprintjs/fingerprintjs@3/dist/fp.min.js"></script>
<script>
    // Load FingerprintJS and generate a device token
    async function getDeviceToken() {
        const fpPromise = FingerprintJS.load();
        const fp = await fpPromise;
        const result = await fp.get();
        return result.visitorId;
    }

    // Set device_token value to the hidden input field when the page loads
    document.addEventListener("DOMContentLoaded", async function() {
        const deviceToken = await getDeviceToken();
        document.getElementById('device_token').value = deviceToken;
    });

    function showHidePassword(element) {
        let status  = $(element).data('show');
        if (!status) {
            $(element).parent().find('input').attr('type', 'text');
            $(element).parent().find('i').attr('class', 'fa fa-eye');
        } else {
            $(element).parent().find('input').attr('type', 'password');
            $(element).parent().find('i').attr('class', 'fa fa-eye-slash');
        }
        $(element).data('show', !status);
    }
</script>
@endsection
