 <!-- sidebar menu area start -->
 @php
     $usr = Auth::guard('admin')->user();
 @endphp

 <div class="sidebar-menu">
    <div class="sidebar-header">
        <div class="logo">
            <a href="{{ route('admin.dashboard') }}">
                <h5 class="text-white">Sistem Informasi Rental Mobil</h5><br>
            </a>
        </div>
    </div>
    <div class="main-menu">
        <div class="menu-inner">
            <nav>
                <ul class="metismenu" id="menu">

                    @if ($usr->can('dashboard.view'))
                    <li class="active">
                        <a href="javascript:void(0)" aria-expanded="true"><i class="ti-dashboard"></i><span>dashboard</span></a>
                        <ul class="collapse">
                            <li class="{{ Route::is('admin.dashboard') ? 'active' : '' }}"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        </ul>
                    </li>
                    @endif
                    @if ($usr->can('mobil.create') || $usr->can('mobil.view') ||  $usr->can('mobil.edit') ||  $usr->can('mobil.delete') || $usr->can('mobil.move'))
                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-clipboard"></i><span>
                            Master Data
                        </span></a>
                        <ul class="collapse {{ Route::is('admin.master.mobil.*') ? 'in' : '' }}">
                            @if ($usr->can('mobil.view'))
                                <li class="{{ Route::is('admin.master.mobil.index')  || Route::is('admin.master.mobil.edit') ? 'active' : '' }}"><a href="{{ route('admin.master.mobil.index') }}"> <i class="fa fa-car"></i> Mobil</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if ($usr->can('mobil.create') || $usr->can('mobil.view') ||  $usr->can('mobil.edit') ||  $usr->can('mobil.delete') || $usr->can('mobil.move'))
                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-refresh fa-spin"></i><span>
                            Transaksi Module
                        </span></a>
                        <ul class="collapse {{ Route::is('admin.roles.create') || Route::is('admin.roles.index') || Route::is('admin.roles.edit') || Route::is('admin.roles.show') ? 'in' : '' }}">
                            @if ($usr->can('role.view'))
                                <li class="{{ Route::is('admin.roles.index')  || Route::is('admin.roles.edit') ? 'active' : '' }}"><a href="{{ route('admin.roles.index') }}"> <i class="fa fa-car"></i> Peminjaman Mobil</a></li>
                            @endif
                            @if ($usr->can('role.view'))
                                <li class="{{ Route::is('admin.roles.index')  || Route::is('admin.roles.edit') ? 'active' : '' }}"><a href="{{ route('admin.roles.index') }}"> <i class="fa fa-car"></i>Pengembalian Mobil</a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                    @if ($usr->can('admin.create') || $usr->can('admin.view') ||  $usr->can('admin.edit') ||  $usr->can('admin.delete') || $usr->can('role.create') || $usr->can('role.view') ||  $usr->can('role.edit') ||  $usr->can('role.delete') || $usr->can('permisions.create') || $usr->can('permisions.view') ||  $usr->can('permisions.edit') ||  $usr->can('permisions.delete') || $usr->can('company.create') || $usr->can('company.view') ||  $usr->can('company.edit') ||  $usr->can('company.delete'))
                    <li>
                        <a href="javascript:void(0)" aria-expanded="true"><i class="fa fa-cog"></i>
                        <span>
                            System Configuration
                        </span></a>
                        <ul class="collapse {{ Route::is('admin.admins.*') || Route::is('admin.roles.*') || Route::is('config.company.edit') || Route::is('config.company.update') || Route::is('admin.permisions.*') ? 'in' : '' }}">
                            {{-- @if ($usr->can('company.view'))
                                <li class="{{ Route::is('config.company.edit',1) ? 'active' : '' }}"><a href="{{ route('config.company.edit', 1) }}">Company Profile</a></li>
                            @endif --}}
                            @if ($usr->can('admin.view'))
                                <li class="{{ Route::is('admin.admins.index')  || Route::is('admin.admins.edit') ? 'active' : '' }}"><a href="{{ route('admin.admins.index') }}"> <i class="fa fa-user"></i>  Pengguna</a></li>
                            @endif
                            @if ($usr->can('role.view'))
                                <li class="{{ Route::is('admin.roles.index')  || Route::is('admin.roles.edit') ? 'active' : '' }}"><a href="{{ route('admin.roles.index') }}"> <i class="fa fa-tag" aria-hidden="true"></i> Role User</a></li>
                            @endif
                            @if ($usr->can('permisions.view'))
                                <li class="{{ Route::is('admin.permisions.index')  || Route::is('admin.permisions.edit') ? 'active' : '' }}"><a href="{{ route('admin.permisions.index') }}"><i class="fa fa-terminal" aria-hidden="true"></i>
                                <span>Permisions Access</span></a></li>
                            @endif
                        </ul>
                    </li>
                    @endif

                </ul>
            </nav>
        </div>
    </div>
</div>
<!-- sidebar menu area end -->