@extends('backend.layouts.master')

@section('title')
Detail Roles - Admin Panel
@endsection

@section('styles')
<link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('admin-content')
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Detail Roles</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('admin.roles.index') }}">All Roles data</a></li>
                    <li><span>Detail Roles - <b>{{$roles->name}}</b></span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<div class="main-content-inner">
    <br>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col col-md-6"><b>Role Details</b></div>
            </div>
        </div>
        <div class="card-body">
            <div class="row mb-3">
                <label class="col-sm-2 col-label-form"><b>Roles Name</b></label>
                <div class="col-sm-10">
                    {{ $roles->name }}
                </div>
            </div>
            <div class="row mb-3">
                <label class="col-sm-2 col-label-form"><b>Permissions</b></label>
                <div class="col-sm-10">
                    @foreach ($roles->permissions as $perm)
                    <span class="badge badge-info mr-1">
                        {{ $perm->name }}
                    </span>
                    @endforeach
                </div>
            </div>


            <div class="pull-right">
                <a class="btn btn-primary btn-sm float-end" style="
                margin-top: -30px;" href="{{ route('admin.roles.index') }}" style="border-radius: 35px;"> <i class="fa fa-eye"></i>  View All</a>
            </div>
        </div>
    </div>
</div>
@endsection