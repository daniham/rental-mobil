@extends('backend.layouts.master')

@section('title')
Company Edit - Admin Panel
@endsection

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('admin-content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Config Company</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><span>Edit Company - {{ $company->company_name }}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Company - {{ $company->company_name }}</h4>
                    @include('backend.layouts.partials.messages')
                    
                    <form action="{{ route('config.company.update', $company->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="company_name">Company Name</label>
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Company Name" value="{{ $company->company_name }}">
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="company_address">Company Address</label>
                                <input type="text" class="form-control" id="company_address" name="company_address" placeholder="Enter Company Name" value="{{ $company->company_address }}">
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="company_email">Company Email</label>
                                <input type="email" class="form-control" id="company_email" name="company_email" placeholder="Enter company_email" value="{{ $company->company_email }}">
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control" id="contact_number" name="contact_number" placeholder="Enter contact_number" value="{{ $company->contact_number }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Update Company</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $('.select2').select2();
    })
</script>
@endsection