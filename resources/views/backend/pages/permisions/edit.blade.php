@extends('backend.layouts.master')

@section('title')
Permissions Edit - Admin Panel
@endsection

@section('styles')
<link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('admin-content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Permissions Edit</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('admin.permisions.index') }}">All Permissions</a></li>
                    <li><span>Edit Permissions - {{ $permisions->name }}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Permissions - {{ $permisions->name }}</h4>
                    @include('backend.layouts.partials.messages')
                    
                    <form action="{{ route('admin.permisions.update', $permisions->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-12">
                                <label for="name">Permissions Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Permissions Name" value="{{ $permisions->name }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-12">
                                <label for="guard_name">Guard Name</label>
                                <input type="text" class="form-control" id="guard_name" name="guard_name" placeholder="guard_name" value="{{ $permisions->guard_name }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-12">
                                <label for="group_name">Group Name</label>
                                <input type="text" class="form-control" id="group_name" name="group_name" placeholder="group_name" value="{{ $permisions->group_name }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4">Updated Permissions</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection