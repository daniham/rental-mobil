@extends('backend.layouts.master')

@section('title')
Permissions - Admin Panel
@endsection

@section('styles')
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
@endsection
@section('admin-content')
{{-- breadcump --}}
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Permissions</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><span>Permissions Data</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.permisions.file-import') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <fieldset>
                            <h4>Import Permissions Data</h4>
                            <small class="warning text-muted">{{__('Please upload only Excel (.xlsx or .xls) files')}}</small>
                            <div class="input-group">
                                <input type="file" required class="form-control" name="uploaded_file" id="uploaded_file">
                                @if ($errors->has('uploaded_file'))
                                    <p class="text-right mb-0">
                                        <small class="danger text-muted" id="file-error">{{ $errors->first('uploaded_file') }}</small>
                                    </p>
                                @endif
                                <div class="input-group-append" id="button-addon2">
                                    <button class="btn btn-primary square" type="submit"><i class="ft-upload mr-1"></i> Import data</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title float-left">Permissions Data</h4>
                    <p class="float-right mb-2">
                        <a class="btn btn-primary text-white" href="{{ route('admin.permisions.create') }}">Add New Permissions</a>
                    </p>
                    
                    <div class="clearfix"></div>
                    <div class="data-tables">
                        @include('backend.layouts.partials.messages')
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="10%">Permissions Name</th>
                                    <th width="10%">Guard Name</th>
                                    <th width="10%">Group Name</th>
                                    <th width="10%">Created By</th>
                                    <th width="10%">Updated By</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach ($permisions as $permisions)
                               <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $permisions->name }}</td>
                                    <td>{{ $permisions->guard_name }}</td>
                                    <td>{{ $permisions->group_name }}</td>
                                    <td>{{ $permisions->created_at }}</td>
                                    <td>{{ $permisions->updated_at }}</td>
                                    <td>
                                        <div class="dropdown-container" style="font-size: 0.5rem;border-radius: 10px;">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                                                href="#" role="button" data-toggle="dropdown">
                                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 16 16" width="16" size="16" height="16" style="font-size:25px;" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                    <circle cx="8" cy="2.5" r=".75"/>
                                                    <circle cx="8" cy="8" r=".75"/>
                                                    <circle cx="8" cy="13.5" r=".75"/>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list" style="border-radius: 15px;">
                                                <a class="dropdown-item"
                                                    href="{{ route('admin.permisions.edit', $permisions->id) }}"><i class="fa fa-pencil"></i> Edit
                                                </a>
                                                <form method="POST" id="delete-form-{{ $permisions->id }}" action="{{ route('admin.permisions.destroy', $permisions->id) }}">
                                                    @csrf
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <a type="submit" class="dropdown-item show_confirm" style="cursor:pointer" data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i> Delete</a>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Are you sure you want to delete this record?`,
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                form.submit();
                }
            });
        });
    </script>
     <!-- Start datatable js -->
     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
     <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
     <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
     <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
     
     <script>
         /*================================
        datatable active
        ==================================*/
        if ($('#dataTable').length) {
            $('#dataTable').DataTable({
                fixedHeader: true,
                fixedRows: true,
                responsive: false,
            });
        }

     </script>
@endsection