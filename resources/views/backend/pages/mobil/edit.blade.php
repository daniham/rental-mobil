@extends('backend.layouts.master')

@section('title')
Mobil Edit - Admin Panel
@endsection

@section('styles')
<link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('admin-content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Mobil Edit</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('admin.master.mobil.index') }}">Mobil Data</a></li>
                    <li><span>Edit Mobil Data - <b>{{$mobil->merek}}</b></span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Edit Mobil  - {{ $mobil->merek }}</h4>
                    @include('backend.layouts.partials.messages')
                    
                    <form action="{{ route('admin.master.mobil.update', $mobil->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        
                        <div class="form-row">
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="merek">Merek <span class="text-danger">*</span></label>
                                <input type="text" id="merek" name="merek" class="form-control" autofocus  onfocus="this.value = this.value;" value="{{$mobil->merek}}" placeholder="Merek Mobil" />
                            </div>
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="model">Model <span class="text-danger">*</span></label>
                                <input type="text" id="model" name="model" class="form-control" autofocus  onfocus="this.value = this.value;" value="{{$mobil->model}}" placeholder="Model Mobil" />
                            </div>
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="plat_nomor">Plat Nomor <span class="text-danger">*</span></label>
                                <input type="text" id="plat_nomor" name="plat_nomor" class="form-control" autofocus  onfocus="this.value = this.value;" value="{{$mobil->plat_nomor}}" placeholder="Plat Nomor Mobil" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="tarif">Tarif <span class="text-danger">*</span></label>
                                <input type="text" id="tarif" name="tarif" class="form-control" autofocus  onfocus="this.value = this.value;" value="@currencyCustome($mobil->tarif)" placeholder="Enter Amount in Rupiah" />
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="status">Mobil data status <span class="text-danger">*</span></label>
                                <select class="form-control selectpicker" data-live-search="true" id="status" name="status">
                                    <option value="">Choose Status</option>
                                    <option value="1" {{ $mobil->status == "1" ? 'selected' : '' }}>Active</option>
                                    <option value="0" {{ $mobil->status == "0" ? 'selected' : '' }}>Deactivate</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12 col-sm-12">
                                <label>Product Picture<span class="text-danger"> | jpeg,png,jpg,gif,svg|max:2MB</span></label>
                                    <img class="form-control" src="{{ asset('public/storage/images/'.$mobil->image) }}" style="height: 100px;width:140px;">
                                    <br>
                                    <input type="file" class="form-control" name="image" @error('image') is-invalid @enderror  value="{{ $mobil->image }}">
                            </div>
                        </div>
                        
                        <button onclick="history.go(-1);" class="btn btn-warning mt-4 pr-4 pl-4" style="border-radius: 30px;">
                            <i class="fa fa-arrow-left"></i> Back to All
                        </button>
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" style="border-radius: 30px;"><i class="fa fa-save"></i> Update</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection
@section('scripts')
    <script>
        // Define function to format Rupiah currency
        function formatRupiah(angka) {
            var number_string = angka.toString().replace(/[^,\d]/g, ''),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);
            
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
            
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return 'Rp ' + rupiah;
        }

        // Define event listener for input field
        document.getElementById('tarif').addEventListener('keyup', function() {
            // Get input value
            var inputValue = this.value;

            // Format input value as Rupiah currency
            var formattedValue = formatRupiah(inputValue);

            // Set formatted value back to input field
            this.value = formattedValue;
        });
    </script>

    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker();    
        });

        function currencyInput(element) {
            const value = $(element).val();
            const replaceValue = value.replace(/\D/g, "");
            const numberValue = !!replaceValue ? parseInt(replaceValue).toLocaleString().replaceAll(',', '.') : "0";

            $(element).val(`Rp ${numberValue}`)
            $(element).parent().find('input').eq(0).val(replaceValue)
        }
    </script>
@endsection