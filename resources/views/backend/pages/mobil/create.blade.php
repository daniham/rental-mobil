@extends('backend.layouts.master')

@section('title')
Mobil - Admin Panel
@endsection

@section('styles')
<link href="{{ asset('public/css/select2.min.css') }}" rel="stylesheet" />

<style>
    .form-check-label {
        text-transform: capitalize;
    }
</style>
@endsection
@section('admin-content')
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Mobil</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('admin.master.mobil.index') }}">Mobil Data</a></li>
                    <li><span>Add Mobil</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">Create Mobil</h4>
                    @include('backend.layouts.partials.messages')
                    
                    <form action="{{ route('admin.master.mobil.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-row">
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="merek">Merek <span class="text-danger">*</span></label>
                                <input type="text" id="merek" name="merek" class="form-control" placeholder="Merek Mobil" />
                            </div>
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="model">Model <span class="text-danger">*</span></label>
                                <input type="text" id="model" name="model" class="form-control" placeholder="Model Mobil" />
                            </div>
                            <div class="form-group col-md-4 col-sm-12">
                                <label for="plat_nomor">Plat Nomor <span class="text-danger">*</span></label>
                                <input type="text" id="plat_nomor" name="plat_nomor" class="form-control" placeholder="plat_nomor Mobil" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 col-sm-12">
                                <label for="tarif">Tarif <span class="text-danger">*</span></label>
                                <input type="text" id="tarif" name="tarif" class="form-control" placeholder="Masukan Angka Rupiah" />
                            </div>
                            <div class="form-group col-md-6 col-sm-12">
                                <label>Gambar Mobil<span class="text-danger"> | jpeg,png,jpg,gif,svg|max:2MB</span></label>
                                <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" accept="image/*">
                                @error('image')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>                            
                        </div>
                        
                        <button type="submit" class="btn btn-primary mt-4 pr-4 pl-4" style="border-radius: 10px;"><i class="fa fa-save"></i>  Save</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection
@section('scripts')
<script>
    // Define function to format Rupiah currency
    function formatRupiah(angka) {
        var number_string = angka.toString().replace(/[^,\d]/g, ''),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return 'Rp ' + rupiah;
    }

    // Define event listener for input field
    document.getElementById('tarif').addEventListener('keyup', function() {
        // Get input value
        var inputValue = this.value;

        // Format input value as Rupiah currency
        var formattedValue = formatRupiah(inputValue);

        // Set formatted value back to input field
        this.value = formattedValue;
    });
</script>

    <script>

        $(document).ready(function () {
            $('.selectpicker').selectpicker();    
        });
    </script>
@endsection