@extends('backend.layouts.master')

@section('title')
Mobil - Panel
@endsection

@section('styles')
    <!-- Start datatable css -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">
@endsection
@section('admin-content')
{{-- breadcump --}}
<!-- page title area start -->
<div class="page-title-area">
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="breadcrumbs-area clearfix">
                <h4 class="page-title pull-left">Mobil</h4>
                <ul class="breadcrumbs pull-left">
                    <li><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                    <li><span>Mobil Data</span></li>
                </ul>
            </div>
        </div>
        <div class="col-sm-6 clearfix">
            @include('backend.layouts.partials.logout')
        </div>
    </div>
</div>
<!-- page title area end -->
<div class="main-content-inner">
    <div class="row">
        <!-- data table start -->
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title float-left">Mobil Data</h4>
                    <p class="float-right mb-2">
                        <a class="btn btn-primary text-white" href="{{ route('admin.master.mobil.create') }}" style="border-radius: 15px;"> <i class="fa fa-plus"></i> Add Mobil</a>
                    </p>
                    
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="select-statuse">Status Data</label>
                                <select class="form-control selectpicker" data-live-search="true" name="status" id="select-status" onchange="push_to_url({status: this.value})">
                                    <option selected disabled>Choose Status Data</option>
                                    <option value="">View All</option>
                                    <option value="1">Active</option>
                                    <option value="0">Deactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="data-tables">
                        @include('backend.layouts.partials.messages')
                        <table id="dataTable" class="text-center w-100">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="10%">Merek</th>
                                    <th width="10%">Model</th>
                                    <th width="10%">Plat Nomor</th>
                                    <th width="10%">Tarif</th>
                                    <th width="10%">status</th>
                                    <th width="10%">Created By</th>
                                    <th width="10%">Updated By</th>
                                    <th width="15%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach ($mobil as $mobil)
                               
                               <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $mobil->merek }}</td>
                                    <td>{{ $mobil->model }}</td>
                                    <td>{{ $mobil->plat_nomor}}</td>
                                    <td>{{ $mobil->tarif }}</td>
                                    
                                    <td>
                                        @if ($mobil->status == 1)
                                            <i class="fa fa-circle text-success font-xs mr-1"></i>
                                            Active
                                        @else
                                            <i class="fa fa-circle text-danger font-xs mr-1"></i>
                                            Deactive
                                        @endif
                                    </td> 
                                    <td>{{ $mobil->created_by }}</td>
                                    <td>{{ $mobil->updated_by }}</td>
                                    <td>
                                        <div class="dropdown-container" style="font-size: 0.5rem;border-radius: 10px;">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle"
                                                href="#" role="button" data-toggle="dropdown">
                                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 16 16" width="16" size="16" height="16" style="font-size:25px;" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                                    <circle cx="8" cy="2.5" r=".75"/>
                                                    <circle cx="8" cy="8" r=".75"/>
                                                    <circle cx="8" cy="13.5" r=".75"/>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list" style="border-radius: 15px;">
                                                <a class="dropdown-item"
                                                    href="{{ route('admin.master.mobil.show', $mobil->id) }}"><i class="fa fa-eye"></i> Show
                                                </a>

                                                <a class="dropdown-item"
                                                    href="{{ route('admin.master.mobil.edit', $mobil->id) }}"><i class="fa fa-pencil"></i> Edit
                                                </a>
                                                
                                                @if ($mobil->statuse==1)
                                                    <form method="POST" id="put-form-{{ $mobil->id }}" action="{{ route('admin.softDelete.mobil', $mobil->id) }}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PUT">
                                                        <a type="submit" class="dropdown-item show_confirmdeac"  style="cursor:pointer" data-toggle="tooltip" title='Deactivate'><i class="fa fa-trash"></i> Deactivate</a>
                                                    </form>
                                                @endif
                                                @if ($mobil->statuse==0)
                                                    <form method="POST" id="put-form-{{ $mobil->id }}" action="{{ route('admin.restore.mobil', $mobil->id) }}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="PUT">
                                                        <a type="submit" class="dropdown-item restore"  style="cursor:pointer" data-toggle="tooltip" title='Restore Data'><i class="fa fa-undo"></i> Restore Data</a>
                                                    </form>
                                                @endif
                                                @if (Auth::guard('admin')->user()->entId == 1)
                                                    <form method="POST" id="delete-form-{{ $mobil->id }}" action="{{ route('admin.master.mobil.destroy', $mobil->id) }}">
                                                        @csrf
                                                        <input name="_method" type="hidden" value="DELETE">
                                                        <a type="submit" class="dropdown-item show_confirm" style="cursor:pointer" data-toggle="tooltip" title='Delete'><i class="fa fa-trash"></i> Delete</a>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- data table end -->
        
    </div>
</div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.show_confirmdeac').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Are you sure you want to delete this record?`,
                text: "If you delete this, data will be to deactivate.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                form.submit();
                }
            });
        });
        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Are you sure you want to delete this record?`,
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                form.submit();
                }
            });
        });
        $('.restore').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Are you sure you want to restore this record?`,
                text: "If you restore this, data will be usable again.",
                icon: "info",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                form.submit();
                }
            });
        });
    </script>
     <!-- Start datatable js -->
     <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
     <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
     <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
     <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
     <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
     <script type="text/javascript">
        function push_to_url(params){
            const current_url = window.location.href;
            var urlParams = new URLSearchParams(window.location.search);

            if(Array.from(urlParams).some(([key, value]) => key === Object.keys(params)[0])){
                urlParams.set(Object.keys(params)[0], params[Object.keys(params)[0]])
                var newurl = window.location.origin + window.location.pathname + "?" + urlParams.toString();
            }else{
                var serializeparams = $.param(params);
                var sparactor = current_url.includes('?') ? '&' : '?';
                var newurl = current_url + sparactor + serializeparams;
            }

            window.location.href = newurl;
        }
     </script>
    <script>
        
        $(document).ready(function () {
            // $('.selectpicker').selectpicker();   

            $('#dataTable').DataTable({
                fixedHeader: true,
                fixedRows: true,
                responsive: false,
                pageLength : 10,

                scrollCollapse: true,
                scrollX: 400,
                fixedColumns: {
                    leftColumns: 1 // Tetapkan jumlah kolom yang ingin ditetapkan di sebelah kiri
                }

            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust()
                .responsive.recalc();
            });    
        });
     </script>
     
@endsection